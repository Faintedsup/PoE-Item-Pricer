'''
Initial setup of database
'''

import sqlite3
import requests
from poe_lib import get_ladder_request

def create_properties_table(connection: sqlite3.Connection):
    query = 'CREATE TABLE IF NOT EXISTS properties (id INTEGER, name TEXT, num_values INTEGER, PRIMARY KEY (id))'
    connection.execute(query)

def create_mods_table(connection: sqlite3.Connection):
    query = 'CREATE TABLE IF NOT EXISTS mods (id INTEGER, desc TEXT, num_values INTEGER, PRIMARY KEY (id))'
    connection.execute(query)

def create_ladder_table(connection: sqlite3.Connection):
    query = 'CREATE TABLE IF NOT EXISTS accounts (rank INTEGER, name TEXT, PRIMARY KEY (rank))'
    connection.execute(query)
    fill_ladder_table(connection)

def create_item_table(connection: sqlite3.Connection):
    query = 'CREATE TABLE IF NOT EXISTS items (id TEXT, owner TEXT, base INTEGER, ilvl INTEGER, properties_json TEXT, mods_json TEXT, prefixes INTEGER, suffixes INTEGER, corrupted BOOL, crafted BOOL, price REAL, currency INTEGER, market_rate REAL, PRIMARY KEY (id), FOREIGN KEY(owner) REFERENCES accounts(name))'
    connection.execute(query)

def ladder_generator(league: str, offset: int, limit: int) -> dict:
    while offset < 15000:
        print(offset)
        yield get_ladder_request(league, offset, limit)
        offset += limit
        if offset + limit > 15000:
            limit = 15000 - offset

def fill_ladder_table(connection: sqlite3.Connection):
    cursor = connection.cursor()
    for val in ladder_generator('Blight', 0, 200):
        for dict_ in val['entries']:
            rank = dict_['rank']
            name = dict_['account']['name']
            cursor.execute(f'INSERT INTO accounts VALUES (?, ?)', (rank, name))
    connection.commit()

if __name__ == '__main__':
    conn = sqlite3.connect('poe_pricer.db')
    create_properties_table(conn)
    create_mods_table(conn)
    create_ladder_table(conn)
    create_item_table(conn)
    conn.close()
