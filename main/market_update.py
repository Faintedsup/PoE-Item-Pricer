'''
Utilises the poe.ninja API to fetch exchange rates for currencies.
'''
import sqlite3
from poe_lib import get_market_request

def create_exchange_table(connection: sqlite3.Connection) -> None:
    query = 'CREATE TABLE IF NOT EXISTS exchange (id INTEGER, name TEXT, chaos_rate REAL, exa_rate REAL, PRIMARY KEY (id))'
    connection.execute(query)

def fill_exchange_table(connection: sqlite3.Connection) -> None:
    market_data = get_market_request('Blight')
    exa_rate = 1 / next((cur['chaosEquivalent'] for cur in market_data['lines'] if cur['currencyTypeName'] == 'Exalted Orb'), None)
    cur_info = [(1, 'Chaos Orb', 1, exa_rate)]

    for cur in market_data['lines']:
        cur_info.append((cur['receive']['get_currency_id'], cur['currencyTypeName'], cur['chaosEquivalent'], cur['chaosEquivalent'] * exa_rate))

    cursor = connection.cursor()
    cursor.executemany('INSERT INTO exchange VALUES (?, ?, ?, ?)', cur_info)
    connection.commit()

def update_exchange_table(connection: sqlite3.Connection) -> None:
    market_data = get_market_request('Blight')
    exa_rate = 1 / next((cur['chaosEquivalent'] for cur in market_data['lines'] if cur['currencyTypeName'] == 'Exalted Orb'), None)
    cur_info = [(1, 1, exa_rate)]

    for cur in market_data['lines']:
        cur_info.append((cur['chaosEquivalent'], cur['chaosEquivalent'] * exa_rate, cur['receive']['get_currency_id']))

    cursor = connection.cursor()
    cursor.executemany('UPDATE exchange SET chaos_rate=?, exa_rate=? WHERE id=?', cur_info)
    connection.commit()
    print('Successfully updated exchange table')

if __name__ == '__main__':
    conn = sqlite3.connect('poe_pricer.db')
    create_exchange_table(conn)
    update_exchange_table(conn)
    conn.close()
