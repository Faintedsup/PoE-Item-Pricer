'''
The purpose of this script is to identify the type id of each property for items.
'''
from time import sleep
import requests
import re
from poe_lib import ItemCategory, timer, mod_to_generic, Currency, Item, prod
from market_update import update_exchange_table
from typing import Tuple, Union
from itertools import count
import sqlite3
import json


def get_stash_request(next_id: str) -> dict:
    payload = {'id': next_id}
    r = requests.get("http://api.pathofexile.com/public-stash-tabs/", params=payload)
    return r.json()

class Feed():
    def __init__(self, next_id: str):
        self.conn = sqlite3.connect('poe_pricer.db')
        self.c = self.conn.cursor()

        update_exchange_table(self.conn)

        self.next_id = next_id
        self.known_mods = {data[0]:data[1] for data in self.c.execute('SELECT desc, id FROM mods')}
        self.known_props = {data[0]:data[1] for data in self.c.execute('SELECT name, id FROM properties')}
        self.known_ids = {data[0]:{'amount': data[1], 'currency': data[2], 'exa_rate': data[3]} for data in self.c.execute('SELECT id, price, currency, market_rate FROM items')}
        self.currency_rates = {data[0]:{'chaos_rate': data[2], 'exa_rate': data[3]} for data in self.c.execute('SELECT * FROM exchange')}
        self.accountNames = {name[0] for name in self.c.execute('SELECT name FROM accounts')}

    def __iter__(self):
        return self

    @timer
    def __next__(self) -> bool:
        feed = get_stash_request(self.next_id)
        if not feed:
            return False
        stash_feed = feed['stashes']

        for stash in stash_feed:
            if stash['public'] == False:
                continue
            if stash['accountName'] not in self.accountNames:
                continue
            owner = stash['accountName']
            for item in stash['items']:
                if item['extended']['category'] not in ItemCategory._member_names_:
                    continue
                if item['frameType'] != 2 or 'note' not in item:
                    continue
                if item['identified'] == False:
                    continue
                if item['id'] in self.known_ids:
                    id_ = item['id']
                    if self.check_price(item['note'], item['id']):
                        self.c.execute('UPDATE items SET price=?, currency=?, market_rate=? WHERE id=?', (self.known_ids[id_]['amount'], self.known_ids[id_]['currency'], self.currency_rates[self.known_ids[id_]['currency']]['exa_rate'], id_))
                    continue
                rare_item = Item(item, owner)
                if not rare_item.evaluate:
                    continue
                rare_item.market_price = self.currency_rates[rare_item.currency]['exa_rate'] * rare_item.price_amount

                self.known_ids[rare_item.id] = {'amount': rare_item.price_amount, 'currency': rare_item.currency, 'exa_rate': rare_item.market_price}

                mods_json = self.dump_mod(rare_item.mods)
                properties_json = self.dump_property(rare_item.properties)
                #(id TEXT, owner TEXT, base INTEGER, ilvl INTEGER, properties_json TEXT, mods_json TEXT, prefixes INTEGER, suffixes INTEGER, corrupted BOOL, crafted BOOL, price REAL, currency INTEGER, market_rate REAL)
                self.c.execute('INSERT INTO items VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', (rare_item.id, rare_item.owner, rare_item.base, rare_item.ilvl, properties_json, mods_json, rare_item.prefixes, rare_item.suffixes, rare_item.corrupted, rare_item.crafted, rare_item.price_amount, rare_item.currency, rare_item.market_price))
        self.conn.commit()
        self.next_id = feed['next_change_id']
        with open('nextid.txt', 'w') as fp:
            fp.write(self.next_id)
        return True

    def dump_property(self, properties: dict) -> str:
        property_dict = {}
        for property_ in properties:
            name = property_['name']
            if property_['values']:
                vals = mod_to_generic(property_['values'][0][0])[1]
            else:
                vals = []
            if name not in self.known_props:
                self.known_props[name] = len(self.known_props)
                self.c.execute('INSERT INTO properties VALUES(?, ?, ?)', (self.known_props[name], name, len(vals)))
            property_dict[name] = vals
        return json.dumps(property_dict)
            
    def dump_mod(self, mods: list) -> str:
        mod_dict = {}
        for mod in mods:
            gen_mod, vals = mod_to_generic(mod)
            if gen_mod not in self.known_mods:
                self.known_mods[gen_mod] = len(self.known_mods)
                self.c.execute('INSERT INTO mods VALUES(?, ?, ?)', (self.known_mods[gen_mod], gen_mod, len(vals)))
            mod_dict[gen_mod] = vals
        return json.dumps(mod_dict)

    def check_price(self, note: str, id_: str) -> Union[bool, tuple]:
        note_data = note.split()
        if len(note_data) != 3:
            return False
        if re.search('[a-zA-Z]', note_data[1]):
            return False
        if '~price' or '~b/o' in note_data[0]:
            cur_str = note_data[2].replace('-', '_')
            if cur_str not in Currency._member_names_:
                return False
            note_data[1] = note_data[1].replace(',', '.')
            amount = prod([float(x) for x in note_data[1].split('/')])
            currency = Currency[cur_str]
            if amount == self.known_ids[id_]['amount'] and currency == self.known_ids[id_]['currency']:
                return False
            self.known_ids[id_]['amount'] = amount
            self.known_ids[id_]['currency'] = currency
        return (amount, currency)



if __name__ == '__main__':
    #'484771262-501462611-473419216-541565450-514500332'
    with open('nextid.txt', 'r') as fp:
        next_id = fp.readline()
    feed = Feed(next_id)

    while next(feed):
        pass
