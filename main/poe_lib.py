from enum import IntEnum
from typing import Tuple, Union
from time import time
from requests import get
import functools
import operator
import re
import sqlite3

#Values based on exchange table in db.
class Currency(IntEnum):
    alt = 6
    fus = 5
    fuse = 5
    alch = 4
    chaos = 1
    gcp = 15
    exa = 2
    chrom = 17
    jew = 11
    chance = 16
    chisel = 10
    scour = 14
    blessed = 18
    regret = 9
    regal = 7
    divine = 3
    vaal = 8
    apprentice_sextant = 46
    journeyman_sextant = 47
    master_sextant = 48
    mir = 22
    mirror = 22
    p = 13
    silver = 12
    orb_of_annulment = 73

def get_stash_request(next_id: str) -> dict:
    payload = {'id': next_id}
    r = get("http://api.pathofexile.com/public-stash-tabs/", params=payload)
    return r.json()

def get_ladder_request(league: str, offset: int, limit: int) -> dict:
    assert 0 < limit <= 200, 'limit too large or small'
    assert 0 <= offset + limit <= 15000, 'out of bounds'
    re = get(f'http://api.pathofexile.com/ladders/{league}?&offset={offset}&limit={limit}')
    return re.json()

def get_market_request(league: str) -> dict:
    re = get(f'https://poe.ninja/api/Data/GetCurrencyOverview?league={league}')
    return re.json()

def prod(factors: list) -> float:
    return functools.reduce(operator.mul, factors, 1)

class ItemCategory(IntEnum):
    amulet = 1
    belt = 2
    ring = 3
    helmet = 4
    gloves = 5
    chest = 6
    shield = 7
    quiver = 8
    boots = 9
    abyss = 10
    twosword = 11
    bow = 12
    dagger = 13
    staff = 14
    claw = 15
    onesword = 16
    wand = 17
    oneaxe = 18
    twoaxe = 19
    sceptre = 20
    onemace = 21
    twomace = 22
    jewels = 23
    accessories = 24
    weapons = 25
    armour = 26
    warstaff = 27
    runedagger = 28
    rod = 29

def debug_print(func):
    def f(*args, **kwargs):
        ret = func(*args, **kwargs)
        print(ret)
        return ret
    return f

#TODO: Make decorator work for function generators.
def timer(func):
    call_count = 0
    total = 0
    def f(*args, **kwargs):
        nonlocal call_count
        nonlocal total
        t_1 = time()
        ret = func(*args, **kwargs)
        t_2 = time()
        call_count += 1
        total += t_2 - t_1
        diff = f'{t_2 - t_1:.2f}'
        mean = f'{total / call_count:.2f}'
        print(f'Iter {str(call_count):<5s} {func.__name__:<10s} elapsed: {diff+"s":<10s} mean: {mean+"s":<10s} total: {total:.2f}s')
        return ret
    return f

def mod_to_generic(mod: str) -> Tuple[str, list]:
    reg = re.compile(r'[-+]?\d*\.\d+|\d+')
    return reg.sub('@', mod), [float(x) for x in reg.findall(mod)]

class Item():
    def __init__(self, item: dict, owner: str):
        self.id = item['id']
        self.owner = owner
        self.note = item['note']
        self.ilvl = item['ilvl']
        self.league = item['league']
        self.socket_groups = None if 'sockets' not in item else self.create_socket_groups(item['sockets'])
        self.base = self.set_base(item['extended'])
        self.prefixes = 0 if 'prefixes' not in item['extended'] else item['extended']['prefixes']
        self.suffixes = 0 if 'suffixes' not in item['extended'] else item['extended']['suffixes']
        self.corrupted = False if 'corrupted' not in item else item['corrupted']
        self.identified = None if 'identified' not in item else item['identified']
        self.elder = False if 'elder' not in item else item['elder']
        self.shaper = False if 'shaper' not in item else item['shaper']
        self.talisman_tier = None if 'talismanTier' not in item else item['talismanTier']
        self.crafted = False if 'craftedMods' not in item else True
        self.properties = self.set_properties(item)
        self.mods = self.set_mods(item)
        self.market_price = None
        self.evaluate = self.get_price_from_note()

    def create_socket_groups(self, sockets: dict) -> dict:
        socket_groups = {'num_sockets': 0}
        for socket in sockets:
            group_num = socket['group']
            if group_num not in socket_groups:
                socket_groups[group_num] = {'colors': '', 'len': 0}
            socket_groups[group_num]['colors'] += socket['sColour']
            socket_groups[group_num]['len'] += 1
            socket_groups['num_sockets'] += 1
        return socket_groups

    def get_price_from_note(self) -> bool:
        note_data = self.note.split()
        if len(note_data) != 3:
            return False
        if re.search('[a-zA-Z]', note_data[1]):
            return False
        if '~price' or '~b/o' in note_data[0]:
            cur_str = note_data[2].replace('-', '_')
            if cur_str not in Currency._member_names_:
                return False
            
            self.price_amount = prod([float(x) for x in note_data[1].split('/')])
            self.currency = Currency[cur_str]
        return True

    def set_base(self, item_extended: dict) -> None:
        if item_extended['category'] in ItemCategory._member_names_:
            if item_extended['category'] == 'jewels' and 'subcategory' not in item_extended:
                return item_extended['category']
            return item_extended['subcategories'][0]


    def set_properties(self, item: dict) -> None:
        properties = []
        if 'additionalProperties' in item:
            properties.extend(item['additionalProperties'])
        if 'properties' in item:
            properties.extend(item['properties'])
        if 'requirements' in item:
            properties.extend(item['requirements'])
        return properties

    def set_mods(self, item: dict) -> None:
        mods = []
        if 'enchantMods' in item:
            mods.extend(item['enchantMods'])
        if 'implicitMods' in item:
            mods.extend(item['implicitMods'])
        if 'explicitMods' in item:
            mods.extend(item['explicitMods'])
        if 'craftedMods' in item:
            mods.extend(item['craftedMods'])
        return mods

if __name__ == '__main__':
    #unit tests here
    sample_mods = ['68% increased Physical Damage',
                    'Adds 7 to 11 Physical Damage',
                    'Skills Chain +1 times',
                    '5.6 Life regenerated per second',
                    '+53 to maximum Life']

    assert mod_to_generic(sample_mods[0]) == ('@% increased Physical Damage', [68])
    assert mod_to_generic(sample_mods[1]) == ('Adds @ to @ Physical Damage', [7, 11])
    assert mod_to_generic(sample_mods[2]) == ('Skills Chain +@ times', [1])
    assert mod_to_generic(sample_mods[3]) == ('@ Life regenerated per second', [5.6])