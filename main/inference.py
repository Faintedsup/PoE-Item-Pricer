import numpy as np
import json
from typing import Union
from poe_lib import ItemCategory, timer
import sqlite3


def get_mods(cursor: sqlite3.Cursor) -> dict:
    return {data[1]:{'id': data[0], 'num_values': data[2]} for data in cursor.execute('SELECT * FROM mods')}

def get_properties(cursor: sqlite3.Cursor) -> dict:
    return {data[1]:{'id': data[0], 'num_values': data[2]} for data in cursor.execute('SELECT * FROM properties')}

def get_prop_style_features(item_props: dict, known_props: dict) -> list:
    prop_features = [0 for x in known_props]
    for prop, vals in item_props.items():
        if known_props[prop]['num_values']:
            prop_features[known_props[prop]['id']] = sum(vals)/len(vals)
            yield (known_props[prop]['id'], sum(vals)/len(vals))
            continue
        yield (known_props[prop]['id'], 1)
    return None

def process_row(row: tuple, known_props: dict, known_mods: dict) -> Union[tuple, None]:
    num_ilvls = 6 #81, 82, 83, 84, 85, 86+

    yield (ItemCategory[row[0]] - 1, 1)
    offset = len(ItemCategory._member_names_) - 1 #-1 so we don't have to do it for all subsequent operations

    diff = row[1] - 81
    if diff >= 0:
        yield (offset + min(num_ilvls, diff), 1)
    offset += num_ilvls

    prop_features = get_prop_style_features(json.loads(row[2]), known_props)
    prop_feature = next(prop_features)
    while prop_feature:
        prop_feature = next(prop_features)
        yield (offset + prop_feature[0], prop_feature[1])
    offset += len(known_props)

    mods_features = get_prop_style_features(json.loads(row[3]), known_mods)
    mod_feature = next(mods_features)
    while mod_feature:
        mod_feature = next(mod_feature)
        yield (offset + mod_feature[0], mod_feature[1])
    offset += len(known_mods)
    
    yield (offset + row[4], 1)
    offset += 3

    yield (offset + row[5], 1)
    offset += 3

    yield (offset, row[6])
    offset += 1

    yield (offset, row[7])

    return None

def item_to_feature_list(cursor: sqlite3.Cursor):
    known_mods = get_mods(cursor)
    known_props = get_properties(cursor)

    cursor.execute('SELECT COUNT(*) FROM items')
    num_rows = cursor.fetchone()
    row_length = len(known_mods) + len(known_props) + len(ItemCategory._member_names_) + 6 + 3 + 3 + 1 + 1 #ilvl, prefix, suffix, corrupt, crafted
    feature_array = np.array([np.zeros(row_length) for row in range(num_rows[0])])
    target_array = np.array([np.zeros(1) for row in range(num_rows[0])])
    for idx, row in enumerate(cursor.execute('SELECT base, ilvl, properties_json, mods_json, prefixes, suffixes, corrupted, crafted, price, currency, market_rate FROM items')):
        if idx % 10000 == 0:
            print(f'{idx} / {num_rows} processed')
        features = process_row(row, known_props, known_mods)
        for feature in features:
            feature_array[idx][feature[0]] = feature[1]
        target_array[idx][0] = row[-1]
    return feature_array, target_array


if __name__ == '__main__':
    conn = sqlite3.connect('poe_pricer.db')
    cursor = conn.cursor()
    features, targets = item_to_feature_list(cursor)

    #TODO: Implement inference




        
